# README #

1) Wymagane:

- serwer aplikacyjny Glassfish (4.1)
- google chrome
- Jdk 1.8
- maven

2) Instalacja: 

- rejestrujemy się na trello w celu uzyskania tokena oraz klucza
- tworzymy 3 tablice w aplikacji trello (TODO, DOING, DONE) ich numery id podmieniamy w klasie CardService w enumie LIST_TYPE
- uruchamiamy glassfish
- konfigurujemy mavenowe properties w pom
- w folderze z aplikacją wykonujemy polecenie "mvn clean verify glassfish:redeploy"

3) Obsługa: 

- przechodzimy to http://localhost:8080/trelloBoard/index.xhtml
- podajemy klucz oraz token (są podane domyślne które otrzymałem podczas rejestracji)
- po przekazaniu prawidłowych następuje synchronizacja z trello
- w trybie zsynchronizowanym możemy dodawać, usuwać i edytować (poprzez przeciągnięcie między kolumnami lub zmiane treści) karty 


Zabrakło czasu na :
 - testy jednostkowew Junit i mockito, testy integracyjne w Arquillianie
 - dodanie walidacji w niektórych elementach aplikacji


