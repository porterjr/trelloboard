package cardManagement;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

import dto.Card;
import dto.Cards;

@Remote(CardProcessRemote.class)
@Stateless
public class CardProcessEjb implements CardProcessRemote {
	private final static String CARD_ID = "id";
	private final static String CARD_NAME = "name";

	//TODO check why cards keeping state when injecting it 
//	@Inject
//	private Cards cards;
	
	@Override
	public Cards parseAndGetCards(String contentToParse) {
		Cards cards = new Cards();
		JsonParser parser = Json.createParser(new StringReader(contentToParse));
		Event event = parser.next();
		List<String> ids = new ArrayList<>();
		List<String> names = new ArrayList<>();
		while (parser.hasNext()) {
			event = parser.next();
			if (isJsonKeyIdOfCard(event, parser)) {

				parser.next();
				String elementValue = parser.getString();
				ids.add(elementValue);
			} else if (isJsonNameOfCard(event, parser)) {

				parser.next();
				String elementValue = parser.getString();
				names.add(elementValue);
			}
		}
		for (int i = 0; i < ids.size(); i++) {
			cards.add(new Card(ids.get(i), names.get(i)));
		}
		return cards;
	}
	
	@Override
	public Card parseAndGetCard(String contentToParse) {
		Card card = new Card();
		JsonParser parser = Json.createParser(new StringReader(contentToParse));
		Event event = parser.next();
		while (parser.hasNext()) {
			event = parser.next();
			if (isJsonKeyIdOfCard(event, parser)) {
				parser.next();
				card.setId(parser.getString());
			} else if (isJsonNameOfCard(event, parser)) {
				parser.next();
				card.setName(parser.getString());
			}
		}
		return card;
	}
	
	private boolean isJsonKeyIdOfCard(Event event, JsonParser parser) {
		if (!event.equals(Event.KEY_NAME))
			return false;
		String elementValue = parser.getString();
		return elementValue.equals(CARD_ID);
	}

	private boolean isJsonNameOfCard(Event event, JsonParser parser) {
		if (!event.equals(Event.KEY_NAME))
			return false;
		String elementValue = parser.getString();
		return elementValue.equals(CARD_NAME);
	}
}
