package cardManagement;

import dto.Card;
import dto.Cards;

public interface CardProcessRemote {

	public Cards parseAndGetCards(String contentToParse);
	
	public Card parseAndGetCard(String contentToParse);
}
