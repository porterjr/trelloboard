package cardManagement;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


@Path("/identifierService")
@Singleton
@Lock(LockType.READ)
public class IdentifierService {

	private String key;
	private String token;

	@PUT
	@Path("/setKey")
	@Lock(LockType.WRITE)
	@Consumes(MediaType.TEXT_PLAIN)
	public void setKey(@QueryParam(value = "key") String key) {
		this.key = key;
	}

	@PUT
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/setToken")
	@Lock(LockType.WRITE)
	public void setToken(@QueryParam(value = "token") String token) {
		this.token = token;
	}

	public String getKey() {
		return this.key;
	}

	public String getToken() {
		return this.token;
	}
}
