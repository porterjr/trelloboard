package cardManagement.services;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;

import cardManagement.IdentifierService;

@ApplicationPath("rest")
public class ApplicationConfig extends Application {

	private final Set<Class<?>> classes;

	public ApplicationConfig() {
		final HashSet<Class<?>> set = new HashSet<>();
		set.add(CardsService.class);
		set.add(IdentifierService.class);
		set.add(MOXyJsonProvider.class);
		set.add(Filter.class);
		classes = Collections.unmodifiableSet(set);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}
}
