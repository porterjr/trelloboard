package cardManagement.services;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cardManagement.CardProcessRemote;
import cardManagement.IdentifierService;
import dto.Card;
import dto.Cards;

@Path("/cardService") 
public class CardsService {

	//TODO check if it will be better to involve interceptors to set below vars
	private String key;
	private String token;
	
	@EJB
	private CardProcessRemote cardProcessEjb;
	
	@EJB
	private IdentifierService identifierService;
	
	@GET
	@Path("/getCards")
	public Response getCards(@QueryParam("listName") String listName) {
		fetchKeyAndToken();
		URI uri = getUri("lists");
		WebTarget target = getTarget(uri);
		Response response = target.path(getCorrespondingListId(listName)).path("cards").queryParam("key", key).queryParam("token", token).request().get();
		if(response.getStatus()!=Response.Status.OK.getStatusCode()) {
			return response;
		}
		String responseContent = response.readEntity(String.class);
		Cards cards = cardProcessEjb.parseAndGetCards(responseContent);
		//.header("Access-Control-Allow-Origin","*")
		return Response.ok(cards).build();
	}
	
	@DELETE
	@Path("/deleteCard")
	public Response deleteCard(@QueryParam("cardId") String cardId) {
		fetchKeyAndToken();
		URI uri = getUri("cards");
		WebTarget target = getTarget(uri);
		Response response = target.path(cardId).queryParam("key", key).queryParam("token", token).request().delete();
		if(response.getStatus()!=Response.Status.OK.getStatusCode()) {
			return response;
		}
		return response;
	}
	
	@POST 
	@Path("/addCard")
	public Response addCard(@QueryParam("listName") String listName, @QueryParam("cardName") String cardName) {
		fetchKeyAndToken();
		URI uri;
		try { 
		uri = new URI("https://api.trello.com/1/lists/"+getCorrespondingListId(listName));
		}
		catch(WebApplicationException | URISyntaxException exc) {
throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		WebTarget target = getTarget(uri);
		Response response = target.path("cards").queryParam("name", cardName).queryParam("key", key).queryParam("token", token).request().post(Entity.entity(cardName, MediaType.TEXT_PLAIN));
		String responseContent = response.readEntity(String.class);
		if(response.getStatus()!=Response.Status.OK.getStatusCode()) {
			return response;
		}
		return Response.ok(responseContent).build();
	}
	
	@PUT
	@Path("/editCard")
	public Response editCard(@QueryParam("cardId") String cardId, @QueryParam("newCardName") String newCardName) {
		fetchKeyAndToken();
		URI uri = getUri("cards");
		WebTarget target = getTarget(uri);
		Response response = target.path(cardId).queryParam("name",newCardName).queryParam("key", key).queryParam("token", token).request().put(Entity.entity(newCardName, MediaType.TEXT_PLAIN));
				
		if(response.getStatus()!=Response.Status.OK.getStatusCode()) {
			return response;
		}
		String responseContent = response.readEntity(String.class);
		Card card = cardProcessEjb.parseAndGetCard(responseContent);
		return Response.ok(card).build();
	}
	
	//TODO merge below method with upper - provide @Default value for listName 
	@PUT
	@Path("/moveCard")
	public Response moveCard(@QueryParam("cardId") String cardId, @QueryParam("newListName") String newListName) {
		fetchKeyAndToken();
		URI uri = getUri("cards");
		WebTarget target = getTarget(uri);
		Response response = target.path(cardId).path("idList").queryParam("value",getCorrespondingListId(newListName)).queryParam("key", key).queryParam("token", token).request().put(Entity.entity(newListName, MediaType.TEXT_PLAIN));
				
		if(response.getStatus()!=Response.Status.OK.getStatusCode()) {
			return response;
		}
		String responseContent = response.readEntity(String.class);
		Card card = cardProcessEjb.parseAndGetCard(responseContent);
		return Response.ok(card).build();
	}
	
	private void fetchKeyAndToken() {
		this.key = identifierService.getKey();
		this.token = identifierService.getToken();
	}
	
	private URI getUri(String... parametersToAdd) {
		StringBuilder strBuilder = new StringBuilder();
		URI uri = null;
		for(String paramter : parametersToAdd) {
			strBuilder.append("/");
			strBuilder.append(paramter);
		}
		try {
			uri = new URI("https://api.trello.com/1"+strBuilder.toString());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return uri;
	}
	
	private WebTarget getTarget(URI uri) {
		Client client = ClientBuilder.newClient();
		return client.target(uri);
	}
	
	
	private String getCorrespondingListId(String listName) {
		if(listName.equals("TODO"))
			return LIST_TYPE.TODO.getListId();
		else if(listName.equals("DONE"))
			return LIST_TYPE.DONE.getListId();
		else if(listName.equals("DOING"))
			return LIST_TYPE.DOING.getListId();
		return LIST_TYPE.UNKNOWN.getListId();
	}
	
	public enum LIST_TYPE {
		TODO("5602d81bc65ff859c4892735"),DONE("5602d81bc65ff859c4892737"),DOING("5602d81bc65ff859c4892736"), UNKNOWN("");
		
		private String id;
		
		LIST_TYPE(String id) {
			this.id = id;
		}
		
		public String getListId() { 
			return this.id;
		}
	}
}
