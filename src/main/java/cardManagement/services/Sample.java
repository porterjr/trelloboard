package cardManagement.services;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

import dto.Card;
import dto.Cards;

public class Sample {
	public static void main(String[] args) {
		Cards cards = new Sample().read();
		System.out.println(cards);
	}

	private Cards read() {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader("src/main/resources/sample.json");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		JsonParser parser = Json.createParser(fileReader);
		Event event = parser.next();
		Cards cards = new Cards();
		List<String> ids = new ArrayList<>();
		List<String> names = new ArrayList<>();
		while (parser.hasNext()) {
			event = parser.next();
			Card card = new Card();
			if (isJsonKeyIdOfCard(event, parser)) {
				
				parser.next();
				String elementValue = parser.getString();
				ids.add(elementValue);
			}
			else if (isJsonNameOfCard(event, parser)) {
				
				parser.next();
				String elementValue = parser.getString();
				names.add(elementValue);
			}
		}
		for(int i=0 ; i<ids.size() ; i++) {
			cards.add(new Card(ids.get(i), names.get(i)));
		}
		return cards;
	}

	private final static String CARD_ID = "id";
	private final static String CARD_NAME = "name";

	private boolean isJsonKeyIdOfCard(Event event, JsonParser parser) {
		if (!event.equals(Event.KEY_NAME))
			return false;
		String elementValue = parser.getString();
		return elementValue.equals(CARD_ID);
	}
	
	private boolean isJsonNameOfCard(Event event, JsonParser parser) {
		if (!event.equals(Event.KEY_NAME))
			return false;
		String elementValue = parser.getString();
		return elementValue.equals(CARD_NAME);
	}

}
