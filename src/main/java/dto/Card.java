package dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Card implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1954295085322248155L;
	private String id;
	private String name;
	
	public Card() 
	{
	}
	
	public Card(String id, String name) 
	{
		this.id = id;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
