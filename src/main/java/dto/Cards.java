package dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso({Card.class})
public class Cards extends ArrayList<Card> {


	public Cards() {
	}


	@XmlElement(name = "card")
	public List<Card> getCards() {
		return this;
	}

	public void setCards(List<Card> cards) {
		this.addAll(cards);
	}
}
